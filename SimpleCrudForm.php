<?php

namespace backend\widgets\SimpleCrud;

use yii\base\View;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

/**
 * SimpleCrudForm is the ActiveForm extends class for SimleCrud functional
 *
 * @author Barkov Roman <barkov.rman@gmail.com>
 */
class SimpleCrudForm extends ActiveForm
{
    /** @var  View View object where form show */
    public $viewObject;
    /** @var  ActiveRecord Data model */
    public $model;
    /** @var array Parent entity data */
    public $parent;
    /** @var string Css classes for styling form */
    public $containerCssClasses = 'scrud-form-input-with-medium';
    /** @var array additional form parameters
     *
     * Example
     *  'addFormParams' => ['autoFocusDisable' => true] - disable autofocus on first element in form
     *  'addFormParams' => ['ownFormTitle' => 'My title'] - set own form title
     */
    public $addFormParams;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->options['id'] = 'edit-form';
        parent::init();

        if ($this->model->isNewRecord) {
            $page_title = \Yii::t('app','Add new record into').' "'.$this->model->modelName.'"';
        } else {
            if ($this->addFormParams['ownFormTitle']) {
                $page_title = $this->addFormParams['ownFormTitle'];
            } else {
                $page_title = $this->model->titleForModel;
            }

        }
        echo $this->render('_form_begin',
            [
                'this' => $this->viewObject,
                'page_title' => $page_title,
                'modelname' => $this->model->modelName,
                'parent' => $this->parent,
                'containerCssClasses' => $this->containerCssClasses,
                'addFormParams' => $this->addFormParams,
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->render('_form_end',[]);
        parent::run();
    }
}