<?php

namespace backend\widgets\SimpleCrud;

/**
 * SimpleCrudARChildInterface interface for AR model (use in SimpleCrud grid) that use parent entity
 *
 * @author Barkov Roman <barkov.rman@gmail.com>
 */
Interface SimpleCrudARChildInterface
{
    /**
     * Get parent entity (usually use method implement relation to parent AR model)
     * @return \yii\db\ActiveQuery
     */
    public function getParentObj();
    
    /**
     * Get parent entity class name
     * @return string
     */
    public static function getParentObjClassName();
    
}