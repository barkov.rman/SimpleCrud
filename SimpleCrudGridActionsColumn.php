<?php
namespace backend\widgets\SimpleCrud;

use kartik\grid\DataColumn as KarticBaseColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * SimpleCrudGridActionsColumn columns object that render urls actions (edit,delete) for SimpleCrud grid
 * Based on KarticBaseColumn
 *
 * @author Barkov Roman <barkov.rman@gmail.com>
 */
class SimpleCrudGridActionsColumn extends KarticBaseColumn
{
    /** @var array Parent entity data*/
    public $parent;

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $parent_dop_param='';
        if ($this->parent) $parent_dop_param='&parent_id='.$this->parent['id'];

        return
             Html::a('', Url::to([$model->tableName().'/edit','id'=>$key]).$parent_dop_param, ['class' => 'glyphicon glyphicon-pencil']).' '
            .Html::a(
                        '',
                        Url::to([$model->tableName().'/delete','id'=>$key]),
                        ['class' => 'glyphicon glyphicon-trash', 'data' => ['confirm' => 'Удалить данную запись?', 'method' => 'post']]
                    );
    }
    
}
