# SimpleCrud

This is part of module SimpleCrud (**old version**).
SimpleCrud module ( for [Yii2 framework](http://www.yiiframework.com/) ) implement CRUD task such us: create Controller, create Views.
You don't need create view for index page.

Your controller it will be very easy.

For Example:
```php
<?php
class CityController extends SimpleCrudController
{
    public function init()
    {
        $this->indexPageTitle = 'Cityes';
        $this->modelClassName = 'backend\models\City';
        $this->controllerName = 'city';
        $this->columnsForIndexPage = ['city_title', 'cyty_desr'];
        $this->searchModelClassName = 'backend\models\CitySearch';
        $this->primaryKeyFieldName = 'city_id';
        parent::init();
    }
}
?>
```

For form edit/new data your views like
```php
<?php $form = SimpleCrudForm::begin(['model' => $model, 'viewObject'=>$this]); ?>
    
    .... common code for ActiveForm 
    
<?php SimpleCrudForm::end(); ?>
```    
