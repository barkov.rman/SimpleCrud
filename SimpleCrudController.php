<?php

namespace backend\widgets\SimpleCrud;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

/**
 * SimpleCrudController is the class that must inherit your Controller
 * After inherit your Controller will have actions: index, edit, delete
 * View index auto generated page that show grid based on kartik\grid\GridView
 *
 * @author Barkov Roman <barkov.rman@gmail.com>
 */
class SimpleCrudController extends Controller
{
    /** @var  string Title for index page */
    public $indexPageTitle;
    /** @var  array Columns (see kartik\grid\GridView) */
    public $columnsForIndexPage;
    /** @var  string Class name for data model */
    public $modelClassName;
    /** @var  string Class name for search data model*/
    public $searchModelClassName;
    /** @var  string Controller name that control data model */
    public $controllerName;
    /** @var  string Field name in data model that contains primary key  */
    public $primaryKeyFieldName;
    /** @var  ActiveDataProvider DataProvider for data model*/
    public $dataProvider;
    /** @var  string Url for action Add new record */
    public $urlForAddNewBtn;
    /** @var  string Field name in data model that contains parent ID. If data depend by parent data */
    public $parentKeyFieldName;

    /**
     * @inheritdoc
     *
     * @throws BadRequestHttpException
     */
    public function init()
    {
        // Check required parameters
        $requiredParams = ['modelClassName', 'controllerName', 'columnsForIndexPage', 'primaryKeyFieldName'];
        foreach ($requiredParams as $key => $propName)
        {
            if ($this->{$propName}){
               unset($requiredParams[$key]);
            }
        }
        
        if (!empty($requiredParams)) {
            throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                'params' => implode(', ', $requiredParams),
            ]));
        }
        
        parent::init();
    }

    /**
     * Get data by parent entity
     *
     * @param string $parentId
     * @return array
     * url - url on edit parent entity
     * label - parent title libel
     * all_this_label - label for this child record
     * all_this_url - url for this child record
     * parent_index_url - url for all record like parent
     * parent_index_label - label for all record like parent
     * id - parent primary key value
     */
    private function getParentData($parentId)
    {
        if ($parentId) {
            $modelClassName = $this->modelClassName;
            /** @var ActiveRecord $parentClassName */
            $parentClassName = $modelClassName::getParentObjClassName();
            $parent['url'] = '/'.$parentClassName::getControllerName().'/edit?'.'id='.$parentId;
            $parent['label'] = $parentClassName::findOne($parentId)->titleForModel;
            $parent['all_this_label'] = $this->indexPageTitle;
            $parent['all_this_url'] = '/'.$this->controllerName.'/index?parent_id='.$parentId;
            $parent['parent_index_label'] = $parentClassName::getModelName();
            $parent['parent_index_url'] = '/'.$parentClassName::getControllerName().'/index';
            $parent['id'] = $parentId;
        } else {
            $parent = null;
        }
        return $parent;
    }

    /**
     * Index action
     *
     * Render View for index action. This is a Grid with filters and base actions
     *
     * @param string $parentId
     * @return string
     */
    public function actionIndex($parentId = null)
    {
        Url::remember(Url::current(), $this->controllerName);
        $parent = $this->getParentData($parentId);

        // if this entity depend by parent entity
        if ($parentId) {
            $modelClassName = $this->modelClassName;
            $query = $modelClassName::find()->where([$this->parentKeyFieldName => $parentId]);
            $this->dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
            $this->urlForAddNewBtn = '/'.$this->controllerName.'/edit?parent_id='.$parentId;
        }
        
        // if you need filter for Grid
        if ($this->searchModelClassName) {
            $searchModel = new $this->searchModelClassName;
            $searchParam = Yii::$app->request->queryParams;

            if ($parentId) {
                $searchClassNameShort = array_pop(explode('\\', $this->searchModelClassName));
                $searchParam[$searchClassNameShort] = ArrayHelper::merge($searchParam[$searchClassNameShort], [$this->parentKeyFieldName => $parentId]);
            };
            $dataProvider = $searchModel->search($searchParam);
        } else {
            $searchModel = null;
            $dataProvider = $this->dataProvider;
        }

        // Get url for button "Add new record"
        $urlForAddNewBtn = $this->urlForAddNewBtn ? $this->urlForAddNewBtn : Url::to( [$this->controllerName.'/edit'] );

        return $this->renderContent(SimpleCrudIndex::widget([
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pageTitle' => $this->indexPageTitle,
            'controllerName' => $this->controllerName,
            'columns' => $this->columnsForIndexPage,
            'urlForAddNewBtn' => $urlForAddNewBtn,
            'parent' => $parent,
        ]));
    }

    /**
     * Edit/New action
     * 
     * @param string $id
     * @param string $parentId
     * @return string
     */
    public function actionEdit($id = null, $parentId = null)
    {
        $parent = $this->getParentData($parentId);
        
        // Edit record
        if ($id) {
            $model = $this->findModel($id);
            $redirectTo = Url::previous($this->controllerName);
        // Create new record
        } else {
            $model = new $this->modelClassName;
            if (method_exists($model, 'getParentObj')) { $redirectTo = Url::previous($this->controllerName); }
            else { $redirectTo = Url::to(['/'.$this->controllerName.'/index','sort' => '-'.$this->primaryKeyFieldName]); }
        };

        // if set reference url
        if (Yii::$app->request->getQueryParam('ref')){$redirectTo = Yii::$app->request->getQueryParam('ref');}

        // if set parent ID
        if ($parentId) $model->{$this->parentKeyFieldName} = $parentId;

        if ($model->load(Yii::$app->request->post())&&$model->save()) {return $this->redirect($redirectTo);}

        return $this->render('_form', ['model' => $model, 'parent' => $parent]);
    }

    /**
     * Delete action
     *
     * @param string $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(Url::previous($this->controllerName));
    }

    /**
     * @inheritdoc
     */
    protected function findModel($id)
    {
        $className = $this->modelClassName;
        if (($model = $className::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
