<?php

namespace backend\widgets\SimpleCrud;

use yii\base\Widget;
use yii\db\ActiveRecord;

/**
 * SimpleCrudIndex is the base class for SimleCrud widget
 *
 * @author Barkov Roman <barkov.rman@gmail.com>
 */
class SimpleCrudIndex extends Widget
{
    /** @var  string Title for grid */
    public $pageTitle;
    /** @var  ActiveRecord Search model */
    public $searchModel;
    /** @var  ActiveDataProvider DataProvider for data model */
    public $dataProvider;
    /** @var  array Columns (see kartik\grid\GridView) */
    public $columns;
    /** @var  string Controller name that control data model */
    public $controllerName;
    /** @var  string Url for action Add new record */
    public $urlForAddNewBtn;
    /** @var array Parent entity data */
    public $parent;


    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('index', 
            [
                'searchModel' => $this->searchModel,
                'dataProvider' => $this->dataProvider,
                'columns' => $this->columns,
                'pageTitle' => $this->pageTitle,
                'controllerName' => $this->controllerName,
                'urlForAddNewBtn' => $this->urlForAddNewBtn,
                'parent' => $this->parent
            ]
        );
    }

}
