<?php

namespace backend\widgets\SimpleCrud;

/**
 * SimpleCrudARChildInterface interface for AR model (use in SimpleCrud grid)
 *
 * @author Barkov Roman <barkov.rman@gmail.com>
 */
interface SimpleCrudARInterface
{
    /**
     * Get title for AR record (Short name for all field. For example - "Full name" for people.)
     * @return string
     */
    public function getTitleForModel();

    /**
     * Get name for AR model (For example - "Drivers")
     * @return string
     */
    public function getModelName();

    /**
     * Get controller name which controls the AR model
     * @return mixed
     */
    public static function getControllerName();
}
